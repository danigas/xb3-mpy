from machine import Pin, ADC
from time import ticks_ms
from doolibs.sound_sensor_config import Sound_Sensor_Config


class Sound_Sensor_Logic:
    SENSOR_ID = 0x04
    SAMPLING_PERIOD_MS = 50

    def __init__(self):
        # machine attributes
        self.binaryPin = Pin('D8', Pin.IN, Pin.PULL_UP)
        self.adcPin = Pin("D3", Pin.ANALOG)
        self.adcEnvelop = ADC("D3")

        # variables for non-blocking behavior
        self.read_envelop_flag = False
        self.hits_th1 = 0  # counter for hits over threshold 1
        self.hits_th2 = 0  # counter for hits over threshold 2
        self.hits_th3 = 0  # counter for hits over threshold 3
        self.hits_th4 = 0  # counter for hits over threshold 4
        self.read_adc_offset_time = None
        self.prev_reading_over_th1 = False
        self.prev_reading_over_th2 = False
        self.prev_reading_over_th3 = False
        self.prev_reading_over_th4 = False
        self.start_window_offset_ms = 0
        self.config = Sound_Sensor_Config()

    def task(self):
        """
        A Hit is when the value read from ADC cross over threshold and returns back
        """
        # compare voltage read with the lower threshold set.
        lower_threshold = min(self.config.th1_volts, self.config.th2_volts, self.config.th3_volts, self.config.th4_volts)
        read_voltage = self.read_voltage()
        if self.read_envelop_flag is False and read_voltage >= lower_threshold:
            self.start()

        if self.read_envelop_flag is True:
            ticks_now = ticks_ms()
            if (ticks_now - self.read_adc_offset_time) >= self.SAMPLING_PERIOD_MS:
                # check thresholds
                [self.hits_th1, self.prev_reading_over_th1] = self.check_threshold(read_voltage, self.config.th1_volts, self.prev_reading_over_th1, self.hits_th1)
                [self.hits_th2, self.prev_reading_over_th2] = self.check_threshold(read_voltage, self.config.th2_volts, self.prev_reading_over_th2, self.hits_th2)
                [self.hits_th3, self.prev_reading_over_th3] = self.check_threshold(read_voltage, self.config.th3_volts, self.prev_reading_over_th3, self.hits_th3)
                [self.hits_th4, self.prev_reading_over_th4] = self.check_threshold(read_voltage, self.config.th4_volts, self.prev_reading_over_th4, self.hits_th4)
                self.read_adc_offset_time = ticks_ms()
            if ticks_now - self.start_window_offset_ms >= self.config.time_window_ms:
                self.read_envelop_flag = False
                print("Sound Sensor: End of sound checking cycle. Hits over threshold: Th1=%d, Th2=%d, Th3=%d, Th4=%d\n" %
                      (self.hits_th1, self.hits_th2,
                       self.hits_th3, self.hits_th4))

    def read_voltage(self):
        adc_value = self.adcEnvelop.read()
        volts_value = (adc_value ** 2) * (self.config.coeff_quadratic_numerator / self.config.coeff_quadratic_denominator) + \
                      adc_value * self.config.coeff_linear_numerator / self.config.coeff_linear_denominator + self.config.coeff_offset
        return round(volts_value, 4)

    def read_binary_pin(self):
        return self.binaryPin.value()

    def start(self):
        self.read_envelop_flag = True
        self.reset_counters()
        self.start_window_offset_ms = ticks_ms()
        self.read_adc_offset_time = ticks_ms() - self.SAMPLING_PERIOD_MS  # to do the first readig a time 0
        print("Sound Sensor: start checking sound cycle")

    def reset_counters(self):
        self.hits_th1 = 0
        self.hits_th2 = 0
        self.hits_th3 = 0
        self.hits_th4 = 0
        self.prev_reading_over_th1 = False
        self.prev_reading_over_th2 = False
        self.prev_reading_over_th3 = False
        self.prev_reading_over_th4 = False

    @staticmethod
    def check_threshold(envelop_volts, threshold, prev_reading_over_threshold, hits):
        # check threshold 1
        if envelop_volts >= threshold and prev_reading_over_threshold is False:
            hits += 1
            prev_reading_over_threshold = True
        elif envelop_volts < threshold:
            prev_reading_over_threshold = False
        return [hits, prev_reading_over_threshold]

    def get_sensor_id(self):
        return self.SENSOR_ID

    def set_calibration(self, cal_id, cal_value):
        self.config.set_calibration(cal_id, cal_value)

    def get_calibration(self, cal_id):
        return self.config.get_calibration(cal_id)

    def set_parameter(self, param_id, param_value):
        self.config.set_parameter(param_id, param_value)

    def get_parameter(self, param_id):
        return self.config.get_parameter(param_id)

    def save_parameters(self):
        self.config.save_parameters()

    def save_calibration(self):
        self.config.save_calibration()

    def print_parameters(self):
        self.config.print_parameters()

    def print_calibration(self):
        self.config.print_calibration()
