from xbee import atcmd


class ModuleInfo:
    # TODO: UPDATE THE FOLLOWING WITH ACTUAL VALUES:
    FW_VERSION = bytearray([1, 0])
    HW_VERSION = bytearray([1, 0])
    FW_ID = bytearray([0x00, 0x4A])
    #-----------------------------------

    DESCRIPTION = bytearray(b"Sound_Sensor_Device_")

    def __init__(self):
        # Load Module Info
        self.fw_vs = self.FW_VERSION
        self.hw_vs = self.HW_VERSION
        self.fw_id = self.FW_VERSION
        self.description = self.DESCRIPTION
        self.uid = atcmd("SH") + atcmd("SL")
        self.own_addr = int.from_bytes(atcmd("SL"), 'big', False) # 4 bytes unique address

    def discover_encoding(self):
        data_encoded = bytearray()
        data_encoded[0:0 + len(self.uid)] = self.uid
        data_encoded[8:8 + len(self.fw_vs)] = self.fw_vs
        data_encoded[10:10 + len(self.hw_vs)] = self.hw_vs
        data_encoded[12:12 + len(self.fw_id)] = self.fw_id
        data_encoded[14:14 + len(self.description)] = self.description
        return data_encoded
