from time import ticks_ms
from doolibs.static_memory import static_mem_save_timestamp, static_mem_read_timestamp


class Clock:
    CHECK_TICKS_PERIOD_MS = 1000
    SAVE_TIMESTAMP_PERIOD_MS = 10000

    def __init__(self):
        self.timestamp_sec = self.read_timestamp()
        self.last_ticks_for_clock_ms = ticks_ms()
        self.last_ticks_for_save_ms = ticks_ms()

    def task(self):
        ticks_now = ticks_ms()
        if ticks_now - self.last_ticks_for_clock_ms >= self.CHECK_TICKS_PERIOD_MS:
            self.timestamp_sec += (ticks_now - self.last_ticks_for_clock_ms) / 1000
            self.last_ticks_for_clock_ms = ticks_now
        if ticks_now - self.last_ticks_for_save_ms >= self.SAVE_TIMESTAMP_PERIOD_MS:
            self.save_timestamp()
            self.last_ticks_for_save_ms = ticks_now

    def get_timestamp(self):
        self.print_timestamp()
        return int(self.timestamp_sec)

    def set_timestamp(self, new_timestamp_sec: int):
        if new_timestamp_sec >= 0:
            self.timestamp_sec = new_timestamp_sec
            self.save_timestamp()
            self.print_timestamp()

    def save_timestamp(self):
        return static_mem_save_timestamp(int(self.timestamp_sec))

    def read_timestamp(self):
        params_list = []
        if static_mem_read_timestamp(params_list):
            return params_list[0]
        else:
            return 0

    def print_timestamp(self):
        print("Timestamp: %d" % int(self.timestamp_sec))
