from doolibs.static_memory import static_mem_read_sound_sensor_calibration, static_mem_save_sound_sensor_calibration, \
                                    static_mem_read_sound_sensor_params, static_mem_save_sound_sensor_params


class Sound_Sensor_Config:
    """Default constants"""
    # PARAMETERS
    HITS_FOR_ALARM = 3
    THRESHOLD_VOLTS = 0.7000
    TIME_WINDOW_MS = 5000

    # CALIBRATION
    COEFF_QUADRATIC_NUMERATOR = 0
    COEFF_QUADRATIC_DENOMINATOR = 1.0
    COEFF_LINEAR_NUMERATOR = 5.0 # this is Vref * Resistor_Divier_Gain = 2.5 * 2
    COEFF_LINEAR_DENOMINATOR = 4095 # 12-bit adc max value read
    COEFF_OFFSET = 0

    # LIMIT VALUES
    MIN_THRESHOLD_VOLTS = 0.7000
    TIME_WINDOW_MAX_MS = 99000
    MAX_THRESHOLD_VOLTS = 3.2000

    def __init__(self):
        # calibration for converting adc reading into volts
        # voltage = (adc**2 * quad_num / quad_denom)  +  (adc  * linear_num / linear_denom)  +  offset

        # load calibration
        cal_list = []
        if static_mem_read_sound_sensor_calibration(cal_list):
            self.coeff_linear_numerator = cal_list[0]
            self.coeff_linear_denominator = cal_list[1]
            self.coeff_offset = cal_list[2]
            self.coeff_quadratic_numerator = cal_list[3]
            self.coeff_quadratic_denominator = cal_list[4]

        else:
            print("Sound Sensor: Loading Default Calibration\n")
            self.coeff_quadratic_numerator = self.COEFF_QUADRATIC_NUMERATOR
            self.coeff_quadratic_denominator = self.COEFF_QUADRATIC_DENOMINATOR
            self.coeff_linear_numerator = self.COEFF_LINEAR_NUMERATOR
            self.coeff_linear_denominator = self.COEFF_LINEAR_DENOMINATOR
            self.coeff_offset = self.COEFF_OFFSET
            self.save_calibration()

        # Load configuration parameters
        params_list = []
        if static_mem_read_sound_sensor_params(params_list) is True:
            self.time_window_ms = params_list[0]
            self.th1_volts = params_list[1]
            self.th2_volts = params_list[2]
            self.th3_volts = params_list[3]
            self.th4_volts = params_list[4]
            self.hits_for_alarm = params_list[5]
        else:
            print("Sound Sensor: Loading Default Parameters\n")
            # mem is corrupted or not configured. Set defaults and save
            self.time_window_ms = self.TIME_WINDOW_MS
            self.th1_volts = self.THRESHOLD_VOLTS
            self.th2_volts = self.THRESHOLD_VOLTS
            self.th3_volts = self.THRESHOLD_VOLTS
            self.th4_volts = self.THRESHOLD_VOLTS
            self.hits_for_alarm = self.HITS_FOR_ALARM
            self.save_parameters()

    def set_calibration(self, cal_id, cal_value):
        if cal_id == 1:  # lineal numerator
            self.coeff_linear_numerator = cal_value
        elif cal_id == 2:  # lineal deniminator
            if cal_value > 0.0000:
                self.coeff_linear_denominator = cal_value
        elif cal_id == 3:  # offset
            self.coeff_offset = cal_value
        elif cal_id == 4:  # quadratic numerator
            self.coeff_quadratic_numerator = cal_value
        elif cal_id == 5:  # quadratic denominator
            if cal_value > 0.0000:
                self.coeff_quadratic_denominator = cal_value

    def get_calibration(self, cal_id):
        cal_list = [0,self.coeff_linear_numerator, self.coeff_linear_denominator, self.coeff_offset, self.coeff_quadratic_numerator, self.coeff_quadratic_denominator]
        if cal_id < len(cal_list):
            return cal_list[cal_id]

    def set_parameter(self, param_id, param_value):
        if param_id == 5:
            param_value *= 1000
            if 0 < param_value <= self.TIME_WINDOW_MAX_MS:
                self.time_window_ms = param_value
        elif param_id == 6:
            param_value_int = int(param_value)
            if 0 < param_value_int <= 10:
                self.hits_for_alarm = int(param_value_int)
        else:
            if self.MIN_THRESHOLD_VOLTS <= param_value < self.MAX_THRESHOLD_VOLTS:
                if param_id == 1:
                    self.th1_volts = param_value
                elif param_id == 2:
                    self.th2_volts = param_value
                elif param_id == 3:
                    self.th3_volts = param_value
                elif param_id == 4:
                    self.th4_volts = param_value

    def get_parameter(self, param_id):
        param_list = [0, self.th1_volts, self.th2_volts, self.th3_volts, self.th4_volts, int(self.time_window_ms / 1000), self.hits_for_alarm]
        if param_id < len(param_list):
            return param_list[param_id]
        else:
            return None

    # save current parameters
    def save_parameters(self):
        self.print_parameters()
        static_mem_save_sound_sensor_params(self.time_window_ms, self.th1_volts, self.th2_volts, self.th3_volts,
                                            self.th4_volts, self.hits_for_alarm)

    def print_parameters(self):
        print("Sound Sensor: PARAMETERS th1=%.4f[V], th2=%.4f[V], th3=%.4f[V], th4=%.4f[V], time_window_ms=%d, "
              "hits_for_alarm=%d\n" % (self.th1_volts, self.th2_volts, self.th3_volts, self.th4_volts, self.time_window_ms,
                                       self.hits_for_alarm))

    # save current calibration
    def save_calibration(self):
        self.print_calibration()
        static_mem_save_sound_sensor_calibration(self.coeff_linear_numerator, self.coeff_linear_denominator,
                                                 self.coeff_offset, self.coeff_quadratic_numerator, self.coeff_quadratic_denominator)

    def print_calibration(self):
        print("Sound Sensor: CALIBRATION Lin Num= %.4f, Lin Den= %.4f, Offset= %.4f, Quad Num= %.4f, Quad Den= %.4f"
              %(self.coeff_linear_numerator, self.coeff_linear_denominator, self.coeff_offset,
                self.coeff_quadratic_numerator, self.coeff_quadratic_denominator))
