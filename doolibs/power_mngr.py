from xbee import XBee, PIN_WAKE
from time import ticks_ms
from doolibs.cmd_mngr import send_awake_msg
from doolibs.metering import Metering
from doolibs.static_memory import static_mem_save_pwr_mngr_params, static_mem_read_pwr_mngr_params


class Power_Mngr(Metering):
    WAKE_TIME_MS = 20000
    SLEEP_TIME_MS = 1000
    BATTERY_VOLTAGE_MIN_V = 3.300

    def __init__(self, wakePinFcn=None, wakeRtcFcn=None, useWakePin=False):
        # default config
        self.wake_timeout_ms = self.WAKE_TIME_MS
        self.sleep_timeout_ms = self.SLEEP_TIME_MS
        self.enabled = True

        self.wake_pin_fcn = wakePinFcn
        self.wake_rtc_fcn = wakeRtcFcn
        self.useWakePin = useWakePin

        self.is_wake_status = None
        self.current_status_time_offset_ms = None

        self.xb = XBee()

        # read parameters from memory and force configuration if params make sense
        if self.read_parameters() is False:
            print("Power Manager: Loading default parameters")
            self.save_parameters()

    def start(self):
        if self.enabled:
            self.current_status_time_offset_ms = ticks_ms()
            self.is_wake_status = True
            print("Power Manager: Wake reason: RTC. Start Wake period %d ms\n" % self.wake_timeout_ms)
            send_awake_msg()

    def stop(self):
        self.current_status_time_offset_ms = None
        self.is_wake_status = None

    def task(self, sys_busy_flag=False):
        if self.is_wake_status is not None and sys_busy_flag is False:
            elapsed_ms = ticks_ms() - self.current_status_time_offset_ms
            if self.is_wake_status is True:
                # system is awake
                if elapsed_ms >= self.wake_timeout_ms:
                    #change status
                    self.is_wake_status = False
                    self.current_status_time_offset_ms = ticks_ms() #ofset for start counting sleep timeout
                    #go sleep
                    self.go_sleep(0)
            else:
                if elapsed_ms <= self.sleep_timeout_ms:
                    # system has been woken up before sleep timeout (wake pin) so reenters sleep mode in order to
                    # complete sleep timeout
                    self.go_sleep(elapsed_ms)
                else:
                    self.start()

    def go_sleep(self, elapsed_ms):
        remaining_sleep_time_ms = self.sleep_timeout_ms - elapsed_ms
        print("Power Manager: Go sleep for %d ms\n" % remaining_sleep_time_ms)
        self.xb.sleep_now(remaining_sleep_time_ms, self.useWakePin)  # re-take sleep cycle
        # wake up
        if self.xb.wake_reason() is PIN_WAKE:
            print("Power Manager: Wake reason: Pin\n")
            if self.wake_pin_fcn is not None:
                self.wake_pin_fcn()
        else:
            if self.wake_rtc_fcn is not None:
                self.wake_rtc_fcn()
            self.start()

    def set_params(self, on_off, wake_time_s, sleep_time_s):
        if wake_time_s >= 5: # at least 5000 ms awake
            self.wake_timeout_ms = wake_time_s * 1000
        if sleep_time_s >= 5: # at least 5000 ms asleep
            self.sleep_timeout_ms = sleep_time_s * 1000
        self.enabled = bool(on_off)
        if on_off == 0:
            self.stop()
        else:
            self.start()
        self.print_params()
        self.save_parameters()

    def get_params(self):
        self.print_params()
        return [int(self.enabled), int(self.wake_timeout_ms / 1000), int(self.sleep_timeout_ms / 1000)]

    def read_battery_voltage(self):
        metering = Metering()
        return metering.read_battery_voltage()

    def save_parameters(self):
        static_mem_save_pwr_mngr_params(int(self.wake_timeout_ms / 1000), int(self.sleep_timeout_ms / 1000), self.enabled)

    def read_parameters(self):
        params_list = []
        if static_mem_read_pwr_mngr_params(params_list):
            self.enabled = bool(params_list[0])
            self.wake_timeout_ms = params_list[1] * 1000
            self.sleep_timeout_ms = params_list[2] * 1000
            return True
        return False

    def print_params(self):
        print("Power Manager: enabled: %d, Wake Time: %d ms, Sleep Time %d ms\n" % (self.enabled, self.wake_timeout_ms,
                                                                                   self.sleep_timeout_ms))
