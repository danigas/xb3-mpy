from doolibs.cmd_mngr import send_alarm
from doolibs.dp1_constants import DP1Constants
from doolibs.utils_misc import parse_int_to_32_bit_byte_array_msb_signed
from time import ticks_ms


class Alarms:
    RESEND_ALARM_MS = 120000

    def __init__(self):
        self.low_bat_sent_offset = 0

    def task(self, sound_sensor_instance, pwr_mngr_instance):
        # Check Sound Alarm
        if sound_sensor_instance.read_envelop_flag is False:
            if sound_sensor_instance.hits_th1 >= sound_sensor_instance.config.hits_for_alarm or \
                    sound_sensor_instance.hits_th2 >= sound_sensor_instance.config.hits_for_alarm or \
                    sound_sensor_instance.hits_th3 >= sound_sensor_instance.config.hits_for_alarm or \
                    sound_sensor_instance.hits_th3 >= sound_sensor_instance.config.hits_for_alarm:
                arguments = [sound_sensor_instance.hits_th1, sound_sensor_instance.hits_th2, sound_sensor_instance.hits_th3,
                             sound_sensor_instance.hits_th4]
                send_alarm(DP1Constants.ALARM_SOUND_SENSOR, arguments)
                sound_sensor_instance.reset_counters()

        # under voltage alarm
        vBat = pwr_mngr_instance.read_battery_voltage()
        if vBat <= pwr_mngr_instance.BATTERY_VOLTAGE_MIN_V:
            if (ticks_ms() - self.low_bat_sent_offset) >= self.RESEND_ALARM_MS:
                arguments = bytearray()
                param_value = int(vBat * 10000)
                param_value_encoded = parse_int_to_32_bit_byte_array_msb_signed(param_value)
                arguments.extend(param_value_encoded)
                send_alarm(DP1Constants.ALARM__UNDER_VOLTAGE, arguments)
                self.low_bat_sent_offset = ticks_ms()