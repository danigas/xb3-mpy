from doolibs.dp1_packet import DP1Packet
from doolibs.dp1_constants import DP1Constants
from doolibs.module_info import ModuleInfo
from doolibs.utils_misc import delay_random
from xbee import transmit, receive, ADDR_BROADCAST
from doolibs.utils_misc import parse_to_32bits_msb_signed, parse_int_to_32_bit_byte_array_msb_signed
from doolibs.metering import Metering
from gc import collect

modInfo = ModuleInfo()


def comm_task(sound_sensor_instance, pwr_mngr_instance, clock_instance, other_class_instances=None):
    # read radio receive buffer
    message_just_received = receive()
    if message_just_received is not None:
        dp1Pkts = DP1Packet.decode(message_just_received['payload'])  # this returns a tuple of DP1 msgs (bytearray)
        if dp1Pkts is not None and len(dp1Pkts) > 0:
            print("Comm Manager: %d Received packets, Destination Address is %d, Command is %d\n" % (len(dp1Pkts),
                                                                                                     dp1Pkts[0].dest,
                                                                                                     dp1Pkts[0].cmd))
            # Execute only if it's a Broadcast (dest = 0) or an Unicast (dest = OWN_ADDRESS)
            if dp1Pkts[0].dest == 0 or dp1Pkts[0].dest == modInfo.own_addr:

                # Discover
                if dp1Pkts[0].cmd == DP1Constants.DEVICE__DISCOVER:
                    print("Comm Manager: Executing discover")
                    arguments = bytearray(dp1Pkts[0].args)
                    if len(arguments) > 0:
                        delay_random(arguments[0] * 1000)
                    sendDP1(dp1Pkts[0], DP1Constants.DEVICE__DISCOVER, modInfo.discover_encoding())

                # Set reading time window and sound threshold levels and hits for trigger alarm
                elif dp1Pkts[0].cmd == DP1Constants.PARAMS__SET_SOUND_SENSOR_PARAMETERS:
                    if sound_sensor_instance is not None:
                        if len(dp1Pkts[0].args) >= 11:  # minimum args length
                            sensor_id = dp1Pkts[0].args[0]
                            params = bytearray(dp1Pkts[0].args[1:])
                            if sensor_id == sound_sensor_instance.get_sensor_id():
                                set_parameters(params, sound_sensor_instance.set_parameter)
                                sound_sensor_instance.save_parameters()
                                send_ok_answer(dp1Pkts[0], DP1Constants.PARAMS__SET_SOUND_SENSOR_PARAMETERS)
                            # elif other sensors

                # Get sound threshold levels, reading time window and hits for alarm
                elif dp1Pkts[0].cmd == DP1Constants.PARAMS__GET_SOUND_SENSOR_PARAMETERS:
                    if sound_sensor_instance is not None:
                        if len(dp1Pkts[0].args) >= 3:  # 3 is min length for getting calibration/parameters
                            sensor_id = dp1Pkts[0].args[0]
                            first_param_to_get = dp1Pkts[0].args[1]
                            nb_of_params = dp1Pkts[0].args[2]
                            parsed_params = bytearray()
                            if sensor_id == sound_sensor_instance.get_sensor_id():
                                parsed_params.append(sensor_id)
                                parsed_params.extend(get_parameters_parser(first_param_to_get, nb_of_params,
                                                                           sound_sensor_instance.get_parameter))
                                sound_sensor_instance.print_parameters()
                                if parsed_params is not None:
                                    sendDP1(dp1Pkts[0], DP1Constants.PARAMS__GET_SOUND_SENSOR_PARAMETERS, parsed_params)

                # Set sound sensor calibration: params NUMERATOR, DENOMINATOR AND OFFSET
                elif dp1Pkts[0].cmd == DP1Constants.PARAMS__SET_SENSOR_CALIBRATION:
                    if sound_sensor_instance is not None:
                        args_len = len(dp1Pkts[0].args)
                        sensor_id = dp1Pkts[0].args[0]
                        params = bytearray(dp1Pkts[0].args[2:])
                        if args_len >= 12 and 0 <= sensor_id <= 4:
                            if sensor_id == sound_sensor_instance.get_sensor_id():
                                set_parameters(params, sound_sensor_instance.set_calibration)
                                sound_sensor_instance.save_calibration()
                                send_ok_answer(dp1Pkts[0], DP1Constants.PARAMS__SET_SENSOR_CALIBRATION)

                # Get sound sensor calibration
                elif dp1Pkts[0].cmd == DP1Constants.PARAMS__GET_SENSOR_CALIBRATION:
                    if sound_sensor_instance is not None:
                        if len(dp1Pkts[0].args) >= 3:  # 3 is min length for getting calibration/parameters
                            sensor_id = dp1Pkts[0].args[0]
                            first_param_to_get = dp1Pkts[0].args[1]
                            nb_of_params = dp1Pkts[0].args[2]
                            parsed_params = bytearray()
                            parsed_params.append(sensor_id)
                            parsed_params.append(0x00)  # todo: cal type 0x00 as it can be Linear or Quadratic
                            if sensor_id == sound_sensor_instance.get_sensor_id():
                                parsed_params.extend(get_parameters_parser(first_param_to_get, nb_of_params,
                                                                           sound_sensor_instance.get_calibration))
                                sound_sensor_instance.print_calibration()
                            # elif other sensors
                            if parsed_params is not None:
                                sendDP1(dp1Pkts[0], DP1Constants.PARAMS__GET_SENSOR_CALIBRATION, parsed_params)

                # get sensor level
                elif dp1Pkts[0].cmd == DP1Constants.PARAMS__GET_SENSOR_LEVEL:
                    if sound_sensor_instance is not None:
                        # TODO: Alarm bits not implemented
                        if len(dp1Pkts[0].args) > 0 and dp1Pkts[0].args[0] \
                                == sound_sensor_instance.get_sensor_id():

                            arguments = bytearray()
                            sensor_id = sound_sensor_instance.get_sensor_id()
                            timestamp_seconds = int(clock_instance.get_timestamp())
                            timestamp_bytes = timestamp_seconds.to_bytes(4, 'big')  # 4 bytes
                            voltage_read_float = sound_sensor_instance.read_voltage()
                            print("Sound Sensor: voltage = %.4f [V]\n" % voltage_read_float)

                            # check second parameter to get the right response format
                            if (len(dp1Pkts[0].args) == 1) or (len(dp1Pkts[0].args) > 1) and (dp1Pkts[0].args[1] == 0):
                                voltage_read_int = int(voltage_read_float * 10000)
                                voltage_read_bytes = parse_int_to_32_bit_byte_array_msb_signed(voltage_read_int)
                                # send response in the standard "sensor level" format
                                arguments.append(sensor_id)
                                arguments.extend(timestamp_bytes)
                                arguments.extend(voltage_read_bytes)
                                arguments.append(0x00)  # TODO: ALARMS BITS NOT IMPLEMENTED

                            # send response as readable string according to BT's requirement => Json, sound in volts
                            elif (len(dp1Pkts[0].args) > 1) and (dp1Pkts[0].args[1] == 1):

                                response_string = "{id:" + str(sensor_id) + ", val:" + \
                                                  str(voltage_read_float) + ", unit: V" \
                                                  + ", tsec:" + str(timestamp_seconds) + "}"
                                arguments = bytearray(response_string)

                            sendDP1(dp1Pkts[0], DP1Constants.PARAMS__GET_SENSOR_LEVEL, arguments)

                elif dp1Pkts[0].cmd == DP1Constants.PARAMS__SET_PWR_MNGR_PARAMETERS:
                    if pwr_mngr_instance is not None:
                        if len(dp1Pkts[0].args) >= 3:
                            pwr_mngr_instance.set_params(dp1Pkts[0].args[0], dp1Pkts[0].args[1], dp1Pkts[0].args[2])
                            send_ok_answer(dp1Pkts[0], DP1Constants.PARAMS__SET_PWR_MNGR_PARAMETERS)

                elif dp1Pkts[0].cmd == DP1Constants.PARAMS__GET_PWR_MNGR_PARAMETERS:
                    if sound_sensor_instance is not None:
                        arguments = bytearray(pwr_mngr_instance.get_params())
                        sendDP1(dp1Pkts[0], DP1Constants.PARAMS__GET_SENSOR_LEVEL, arguments)

                elif dp1Pkts[0].cmd == DP1Constants.METERING__GET_METERING:
                    metering = Metering()
                    arguments = bytearray(dp1Pkts[0].args)
                    if metering.check_rx(arguments):
                        answer = metering.encode_tx()
                        sendDP1(dp1Pkts[0], DP1Constants.METERING__GET_METERING, answer)

                elif dp1Pkts[0].cmd == DP1Constants.DEVICE__SYNC_DATE_TIME:
                    if len(dp1Pkts[0].args) >= 4:
                        new_timestamp = parse_to_32bits_msb_signed(dp1Pkts[0].args, 0)
                        clock_instance.set_timestamp(new_timestamp)
                        send_ok_answer(dp1Pkts[0], DP1Constants.DEVICE__SYNC_DATE_TIME)
            collect()


def sendDP1(dp1_pkt: DP1Packet, cmd: int, arguments: bytearray):
    dp1ToSend = DP1Packet(modInfo.own_addr, dp1_pkt.src, cmd, tuple(arguments), 2, 1, False, 1, 1,
                          dp1_pkt.label)
    msgDp1_bytes = dp1ToSend.encode()
    transmit(ADDR_BROADCAST, msgDp1_bytes)


# actually sends discover answer for now
def send_awake_msg():
    arguments = modInfo.discover_encoding()
    dp1ToSend = DP1Packet(modInfo.own_addr, 0, DP1Constants.DEVICE__DISCOVER, arguments, 2, 1, False, 1, 1, 0)
    bytesToSend = dp1ToSend.encode()
    transmit(ADDR_BROADCAST, bytesToSend)


def send_alarm(alarm_id, args):
    # encode arguments:
    # nb of hits
    # timestamp (secs since 00:00:00 from today?)
    dp1ToSend = DP1Packet(modInfo.own_addr, 0, alarm_id, args, 2, 1, False,
                          1, 1, 0)
    msgDp1_bytes = dp1ToSend.encode()
    transmit(ADDR_BROADCAST, msgDp1_bytes)
    alarm_str = ""
    if alarm_id == DP1Constants.ALARM_SOUND_SENSOR:
        alarm_str = "Sound"
    # elif other alarms
    elif alarm_id == DP1Constants.ALARM__UNDER_VOLTAGE:
        alarm_str = "Low Voltage"
    print("Comm Manager: Sending " + alarm_str + " alarm")


def get_parameters_parser(first_param_to_get, nb_of_params, get_param_fnc):
    parsed_params = bytearray()
    real_nb_of_params = 0
    for x in range(first_param_to_get, first_param_to_get + nb_of_params):
        param_id = x
        param = get_param_fnc(param_id)
        if param is not None:
            param_value = int(param * 10000)
            param_value_encoded = parse_int_to_32_bit_byte_array_msb_signed(param_value)
            parsed_params.append(param_id)  # param ID
            parsed_params.extend(param_value_encoded)  # encoded value
            real_nb_of_params += 1
    parsed_params.append(0xFF)  # end of params
    nd_of_params_arr = parse_int_to_32_bit_byte_array_msb_signed(real_nb_of_params * 1000)
    parsed_params.extend(nd_of_params_arr)
    return parsed_params


def set_parameters(arguments, set_param_fcn):
    arg_len = len(arguments)
    num_of_params = int(parse_to_32bits_msb_signed(arguments, arg_len - 4) / 1000)
    required_len = (num_of_params + 1) * 5
    # check message integrity
    if arguments[arg_len - 5] == 0xFF and arg_len == required_len:
        for x in range(0, num_of_params):
            param_id = arguments[x * 5]
            param_val = parse_to_32bits_msb_signed(arguments, 1 + (x * 5)) / 10000
            set_param_fcn(param_id, param_val)


def send_ok_answer(dp1pkt, command):
    answer = bytearray([0x01, ])
    sendDP1(dp1pkt, command, answer)
