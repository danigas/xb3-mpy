from doolibs.utils_misc import parse_to_32bits_msb_signed, parse_int_to_32_bit_byte_array_msb_signed, checksum_16bits
from doolibs.i2c_mem import I2C_Mem

MEMORY_SIZE_BYTES = 1024
PAGE_SIZE_BYTES = 16

# Memory organization
SOUND_SENSOR_PARAMS_ADDRESS = 0
SOUND_SENSOR_PARAMS_LENGTH = 20
SOUND_SENSOR_PARAMS_NB_OF_PAGES = int(SOUND_SENSOR_PARAMS_LENGTH / PAGE_SIZE_BYTES) + 1

SOUND_SENSOR_CALIBRATION_ADDRESS = SOUND_SENSOR_PARAMS_ADDRESS + SOUND_SENSOR_PARAMS_NB_OF_PAGES * PAGE_SIZE_BYTES
SOUND_SENSOR_CALIBRATION_LENGTH = 22
SOUND_SENSOR_CALIBRATION_NB_OF_PAGES = int(SOUND_SENSOR_CALIBRATION_LENGTH / PAGE_SIZE_BYTES) + 1

PWR_MNGR_PARAMS_ADDRESS = SOUND_SENSOR_CALIBRATION_ADDRESS + SOUND_SENSOR_CALIBRATION_NB_OF_PAGES * PAGE_SIZE_BYTES
PWR_MNGR_PARAMS_LENGTH = 5
PWR_MNGR_PARAMS_NB_OF_PAGES = int(PWR_MNGR_PARAMS_LENGTH / PAGE_SIZE_BYTES) + 1

TIMESTAMP_ADDRESS = PWR_MNGR_PARAMS_ADDRESS + PWR_MNGR_PARAMS_NB_OF_PAGES * PAGE_SIZE_BYTES
TIMESTAMP_LENGTH = 6
TIMESTAMP_NB_OF_PAGES = int(TIMESTAMP_LENGTH / PAGE_SIZE_BYTES) + 1


def static_mem_save_sound_sensor_params(time_window_msec, th1, th2, th3, th4, hits_for_alarm=2):
    """
    Memory organization for sound sensor parameters (address 0 means address offset):
    address 0: time_window_sec 1 byte
    address 1: th1  4 bytes
    address 5: th2 4 bytes
    address 9: th3 4 bytes
    address 13: th4 4 bytes
    address 17: hits_for_alarm 1 byte
    address 18: memory block check sum 2 bytes
    """
    # parse byte array to save
    bytes_to_save = bytearray()
    bytes_to_save.append(int(time_window_msec / 1000))
    bytes_to_save.extend(parse_int_to_32_bit_byte_array_msb_signed(int(th1 * 10000)))
    bytes_to_save.extend(parse_int_to_32_bit_byte_array_msb_signed(int(th2 * 10000)))
    bytes_to_save.extend(parse_int_to_32_bit_byte_array_msb_signed(int(th3 * 10000)))
    bytes_to_save.extend(parse_int_to_32_bit_byte_array_msb_signed(int(th4 * 10000)))
    bytes_to_save.append(hits_for_alarm)
    # add check sum to array
    return _append_checksum_and_write(bytes_to_save, SOUND_SENSOR_PARAMS_ADDRESS)


def static_mem_read_sound_sensor_params(params_list: list):
    """
    Memory organization for sound sensor parameters (address 0 means address offset):
    address 0: time_window_sec 1 byte
    address 1: th1  4 bytes
    address 5: th2 4 bytes
    address 9: th3 4 bytes
    address 13: th4 4 bytes
    address 17: hits_for_alarm 1 byte
    address 18: memory block checksum 2 bytes
    """
    bytes_read = bytearray()
    if _basic_read_array(bytes_read, SOUND_SENSOR_PARAMS_ADDRESS, SOUND_SENSOR_PARAMS_LENGTH):
        params_list.append(bytes_read[0] * 1000)
        params_list.append(parse_to_32bits_msb_signed(bytes_read, 1) / 10000)
        params_list.append(parse_to_32bits_msb_signed(bytes_read, 5) / 10000)
        params_list.append(parse_to_32bits_msb_signed(bytes_read, 9) / 10000)
        params_list.append(parse_to_32bits_msb_signed(bytes_read, 13) / 10000)
        params_list.append(bytes_read[17])
        return True
    return False


def static_mem_save_sound_sensor_calibration(lin_num, lin_den, offset, quad_num, quad_den):
    """
    Memory organization for sound sensor parameters (address 0 means address offset):
    address 0: linear numerator 4 bytes
    address 4: linear denominator 4 bytes
    address 8: offset 4 bytes
    address 12: quadratic numerator 4 bytes
    address 16: quadratic denominator 4 bytes
    address 20: memory block check sum 2 bytes
    total 22 bytes
    """
    # parse byte array to save
    bytes_to_save = bytearray()
    bytes_to_save.extend(parse_int_to_32_bit_byte_array_msb_signed(int(lin_num * 10000)))
    bytes_to_save.extend(parse_int_to_32_bit_byte_array_msb_signed(int(lin_den * 10000)))
    bytes_to_save.extend(parse_int_to_32_bit_byte_array_msb_signed(int(offset * 10000)))
    bytes_to_save.extend(parse_int_to_32_bit_byte_array_msb_signed(int(quad_num * 10000)))
    bytes_to_save.extend(parse_int_to_32_bit_byte_array_msb_signed(int(quad_den * 10000)))
    # add check sum to array
    return _append_checksum_and_write(bytes_to_save, SOUND_SENSOR_CALIBRATION_ADDRESS)


def static_mem_read_sound_sensor_calibration(params_list: list):
    """
    Memory organization for sound sensor parameters (address 0 means address offset):
    address 0: linear numerator 4 bytes
    address 4: linear denominator 4 bytes
    address 8: offset 4 bytes
    address 12: quadratic numerator 4 bytes
    address 16: quadratic denominator 4 bytes
    address 20: memory block check sum 2 bytes
    total 22 bytes
    """
    bytes_read = bytearray()
    if _basic_read_array(bytes_read, SOUND_SENSOR_CALIBRATION_ADDRESS, SOUND_SENSOR_CALIBRATION_LENGTH):
        params_list.append(parse_to_32bits_msb_signed(bytes_read, 0) / 10000)
        params_list.append(parse_to_32bits_msb_signed(bytes_read, 4) / 10000)
        params_list.append(parse_to_32bits_msb_signed(bytes_read, 8) / 10000)
        params_list.append(parse_to_32bits_msb_signed(bytes_read, 12) / 10000)
        params_list.append(parse_to_32bits_msb_signed(bytes_read, 16) / 10000)
        return True
    return False


def static_mem_save_pwr_mngr_params(wake_time_secs: int, sleep_time_secs: int, pwr_mngr_on_off: bool):
    """
    Memory organization for power manager parameters (address 0 means address offset):
    address 0: power manager on/off 1 byte
    address 1: wake time (seconds) 1 byte
    address 2: sleep time 1 byte
    address 3: checksum 2 bytes
    total 5 bytes
    """
    bytes_to_save = bytearray([int(pwr_mngr_on_off), wake_time_secs, sleep_time_secs])
    return _append_checksum_and_write(bytes_to_save, PWR_MNGR_PARAMS_ADDRESS)


def static_mem_read_pwr_mngr_params(params_list: list):
    """
    Memory organization for power manager parameters (address 0 means address offset):
    address 0: power manager on/off 1 byte
    address 1: wake time (seconds) 1 byte
    address 2: sleep time 1 byte
    address 3: checksum 2 bytes
    total 5 bytes
    """
    # test address:
    bytes_read = bytearray()
    if _basic_read_array(bytes_read, PWR_MNGR_PARAMS_ADDRESS, PWR_MNGR_PARAMS_LENGTH):
        # encoding
        params_list.append(bytes_read[0])
        params_list.append(bytes_read[1])
        params_list.append(bytes_read[2])
        return True
    return False


def static_mem_save_timestamp(timestamp_sec: int):
    """
    Memory organization for timestamp (address 0 means address offset):
    address 0: timestamp 4 byte
    address 4: checksum 2 bytes
    total 6 bytes
    """
    # parse byte array to save
    bytes_to_save = bytearray()
    bytes_to_save.extend(parse_int_to_32_bit_byte_array_msb_signed(timestamp_sec))
    return _append_checksum_and_write(bytes_to_save, TIMESTAMP_ADDRESS)


def static_mem_read_timestamp(params_list: list):
    """
    Memory organization for timestamp (address 0 means address offset):
    address 0: timestamp 4 byte
    address 4: checksum 2 bytes
    total 6 bytes
    """
    bytes_read = bytearray()
    if _basic_read_array(bytes_read, TIMESTAMP_ADDRESS, TIMESTAMP_LENGTH):
        params_list.append(parse_to_32bits_msb_signed(bytes_read, 0))
        return True
    return False


def _basic_read_array(bytes_read: bytearray, address: int, length: int):
    i2c = I2C_Mem(MEMORY_SIZE_BYTES, PAGE_SIZE_BYTES)
    bytes_read.extend(i2c.read_array(address, length))
    if len(bytes_read) == length:
        check_sum_calculated = checksum_16bits(bytes_read[0:(length - 2)])
        check_sum_read = (bytes_read[length - 2] << 8) + bytes_read[length - 1]
        if check_sum_calculated == check_sum_read:
            return True
    return False


def _append_checksum_and_write(bytes_to_save: bytearray, address: int):
    check_sum = checksum_16bits(bytes_to_save)
    bytes_to_save.append(check_sum >> 8)
    bytes_to_save.append(check_sum & 0xFF)
    i2c = I2C_Mem(MEMORY_SIZE_BYTES, PAGE_SIZE_BYTES)
    return i2c.write_array(address, bytes_to_save)

