from machine import rng
from time import sleep_us


INT32_MAX = 0x7FFFFFF
UINT32_MAX = 0xFFFFFFFF

"""
UNUSED

def iterate(s):
    """"""
    Use this function when you want to use the "for x in s" and
    s could be a single value. This function will make sure you can
    iterate over an iterable and a non-iterable alike.
    """"""
    try:
        return s.__iter__()
    except AttributeError:
        return (s,).__iter__()


def reverse_dict(d):
    """"""Returns a dict where keys and values have reversed roles""""""
    return {v: k for k, v in d.items()}

class StringManip:
    @staticmethod
    def isHex(string):
        """"""
        Check if a string is hexadecimal.
        :param string: The string to check.
        :return: True: The string is HEX, False: The string is NOT HEX.
        """"""
        try:
            int(string, 16)
            return True
        except ValueError:
            return False

    @staticmethod
    def bytearrayToString(array, separator):
        """"""

        :param array:
        :param separator:
        :return:
        """"""
        data = "".join(("{:02x}" + separator).format(x) for x in array)
        if len(separator) != 0:
            return str(data).upper()[: -len(str(separator))]
        return str(data).upper()

"""


def byteArrayInsertElement(idx, targetArray, insertElement):
    """
    Since micropython has no bytearray.insert() method
    :param idx: index from insertArray will be inserted into  targetArray
    :param targetArray: the that is being modified
    :param insertElement: the element that will be insert
    """
    last_element = targetArray[len(targetArray) - 1]
    targetArray.append(last_element)
    new_len = len(targetArray)
    for x in range(new_len - 1, idx, -1):
        targetArray[x] = targetArray[x - 1]
    targetArray[idx] = insertElement


def convertIntToAscii(intNum, nbOfDigits):
    """
    converts an integer to its ascii representation
    :param intNum: the number to be converted
    :param nbOfDigits: the number of digits for the representation (completed with zeros)
    :return:
    """
    if nbOfDigits > 0:
        digitCount = 0
        number = intNum
        zeros_string = ""
        while number > 0:
            number = int(number / 10)
            digitCount += 1

        if nbOfDigits >= digitCount:
            for x in range(0, nbOfDigits - digitCount):
                zeros_string += "0"
        else:
            return None

        return zeros_string + str(intNum)


def parse_to_32bits_msb(pArray, pIndex=0):
    """
    Converts a byte array to a 32bits int, last byte of the array being the most significant.

    :param pArray: byte array to parse
    :param pIndex: index where to start parsing
    :return: 32bits int
    """
    parsedValue = 0
    if pArray is not None and len(pArray) >= (pIndex + 4):
        parsedValue |= (castUnsignedByte(pArray[pIndex]) << 24)
        parsedValue |= (castUnsignedByte(pArray[pIndex + 1]) << 16)
        parsedValue |= (castUnsignedByte(pArray[pIndex + 2]) << 8)
        parsedValue |= (castUnsignedByte(pArray[pIndex + 3]))
    return parsedValue


def parse_to_32bits_msb_signed(payload, array_index):
    """
    Converts a byte array to a 32bits signed int, last byte of the array being the most significant.

    :type payload: byte array
    :type array_index: int
    :return: int
    """
    parsed_value = parse_to_32bits_msb(payload, array_index)

    if parsed_value > INT32_MAX:
        # Convert 32-bits to negative
        parsed_value -= (UINT32_MAX + 1)

    return parsed_value


def castUnsignedByte(value):
    return value % 2 ** 8


def parseIntTo32BitByteArrayMsb(dataToParse):
    """
    Converts a 32bits int to a byte array, with the most significant byte being in the first position.
    :param dataToParse: 32 bit int
    :return: byte array
    """
    transformedValue = bytearray(4)

    transformedValue[0] = castUnsignedByte(dataToParse >> 24)
    transformedValue[1] = castUnsignedByte(dataToParse >> 16)
    transformedValue[2] = castUnsignedByte(dataToParse >> 8)
    transformedValue[3] = castUnsignedByte(dataToParse)

    return transformedValue


def parse_int_to_32_bit_byte_array_msb_signed(value):
    """
    Converts a signed 32bits int to a byte array, with the most significant byte being in the first position.
    :type value: int
    :return: byte array
    """
    if value < 0:
        # Make value positive
        value += UINT32_MAX + 1

    return parseIntTo32BitByteArrayMsb(value)

def delay_random(max_delay_sec_ms):
    random_max_delay_us = max_delay_sec_ms * 1000
    random_number = rng()  # 30 bits (1073741824)
    random_timeout_us = random_number * random_max_delay_us / 1073741824  # this gives the randon number as a proportion of random_max_delay_us
    sleep_us(int(random_timeout_us))  # random blocking wait


def parseTo16BitsMsb(pArray, pIndex=0):
    """
     Converts a an array of bytes to an integer, with the most significant byte being at the index 0.

    :param pArray: array of bytes to parse
    :param pIndex: Index of first byte to parse. Default is 0
    :return: int
    """
    parsedValue = 0
    if pArray is not None and len(pArray) >= (pIndex + 2):
        parsedValue |= (pArray[pIndex] << 8)
        parsedValue |= (pArray[pIndex + 1] << 0)
    return parsedValue


def parseIntTo16BitByteArrayMsb(dataToParse):
    """
    Converts a 32bits int to a byte array, with the most significant byte being in the first position.
    :param dataToParse: 32 bit int
    :return: byte array
    """
    transformedValue = bytearray(2)

    transformedValue[0] = castUnsignedByte(dataToParse >> 8)
    transformedValue[1] = castUnsignedByte(dataToParse)

    return transformedValue


def parseIntTo16BitByteArrayMsb(dataToParse):
    """
    Converts a 32bits int to a byte array, with the most significant byte being in the first position.
    :param dataToParse: 32 bit int
    :return: byte array
    """
    transformedValue = bytearray(2)

    transformedValue[0] = castUnsignedByte(dataToParse >> 8)
    transformedValue[1] = castUnsignedByte(dataToParse)

    return transformedValue


def checksum_16bits(bytes_array):
    chcksm = 0
    for byte in bytes_array:
        chcksm += byte
    chcksm &= 0xFFFF  # 2-bytes checksum
    chcksm = 0xFFFF - chcksm
    return chcksm