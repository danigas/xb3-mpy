from xbee import atcmd
from doolibs.cmd_mngr import comm_task
from doolibs.sound_sensor_logic import Sound_Sensor_Logic
from gc import collect
from doolibs.power_mngr import Power_Mngr
from doolibs.alarms import Alarms
from doolibs.clock import Clock


# ----------- FUNCTIONS --------------#
def init_system():
    # SLEEP MODE = 6 (MICROPYTHON CODE CONTROLS SLEEP)
    atcmd("SM", 6)
    # MICROPYTHIN AUTOSTART = 1 (ON)
    atcmd("PS", 1)
    # UART MODE = 4 (MICROPYTHON REPL MODE) needed for debug console OR controlling a device via uart (gps for instance)
    atcmd("AP", 4)
    # TX POWER = 4 (MAX)
    atcmd("PL", 4)
    # Set internal voltage reference to 2.5V:
    atcmd("AV", 1)
    # Radio Channel
    atcmd("CH", 0x0C)
    # Encryption disabled
    atcmd("EE", 0)
    # Encryption key
    #atcmd("KY", bytearray([0x11, 0xAA, 0x22, 0xBB, 0x33, 0xCC]))
    # Save parameters
    atcmd("WR")


# INSTANCES
sound_sensor = Sound_Sensor_Logic()
power_manager = Power_Mngr(useWakePin=True)
alarms = Alarms()
time_keeping = Clock()


# --------- INIT --------#
init_system()
power_manager.start()

# -------MAIN LOOP-------#
while True:
    sound_sensor.task()

    alarms.task(sound_sensor, power_manager)

    comm_task(sound_sensor, power_manager, time_keeping)  # params: sound, alarm, clock

    # argument: can make an OR with some flags to prevent power manager from going sleep
    power_manager.task(sound_sensor.read_envelop_flag)

    time_keeping.task()

    collect()  # gc module: garbage collection task
